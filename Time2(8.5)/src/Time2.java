
public class Time2
{
   private int second; 
   
   
   public Time2()
   {
      this( 0, 0, 0 ); 
   } 

   public Time2( int h ) 
   { 
      this( h, 0, 0 ); 
   } 
   public Time2( int h, int m ) 
   { 
      this( h, m, 0 );
   }

   public Time2( int h, int m, int s ) 
   { 
      setTime( h, m, s );
   } 
  
   public Time2( Time2 time )
   {
   
      this( time.getHour(), time.getMinute(), time.getSecond() );
   }

   public void setTime( int h, int m, int s )
   {
      setHour( h );   // set the hour
      setMinute( m ); // set the minute
      setSecond( s ); // set the second
   } 
   public void setHour( int h ) 
   { 
     int  hour = ( ( h >= 0 && h < 24 ) ? h : 0 ); 
     second= hour*3600 + getMinute()*60 + getSecond();
   } 
   public void setMinute( int m ) 
   { 
     int minute = ( ( m >= 0 && m < 60 ) ? m : 0 ); 
     second= getHour()*3600 + minute*60 + getSecond();
   } 
   public void setSecond( int s ) 
   { 
	  int se = ( ( s >= 0 && s < 60 ) ? s : 0 );
	  second=getHour()*3600+getMinute()*60 + se;
   } 
   public int getHour() 
   { 
      return this.second/3600; 
   } 
   public int getMinute() 
   { 
      return (this.second-getHour()*3600)/60;
   } 
   public int getSecond() 
   { 
      return  this.second-getHour()*3600-getMinute()*60;
   }
   public String toUniversalString()
   {
      return String.format( 
         "%02d:%02d:%02d", getHour(), getMinute(), getSecond() );
   }
   public String toString()
   {
      return String.format( "%d:%02d:%02d %s", 
         ( (getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12 ),
         getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ) );
   } 
   public static void main(String[] args)
   {
	   Time2 time=new Time2(2,3,4);
	   System.out.print(time);
   }
} // end class Time2