public class Time2
{
   private int hour;   // 0 - 23
   private int minute; // 0 - 59
   private int second; // 0 - 59

   public Time2()
   {
      this( 0, 0, 0 ); // invoke Time2 constructor with three arguments
   } 
   public Time2( int h ) 
   { 
      this( h, 0, 0 ); 
   } 
   public Time2( int h, int m ) 
   { 
      this( h, m, 0 ); 
   } 
   
   public Time2( int h, int m, int s ) 
   { 
      setTime( h, m, s ); 
   } 

   public Time2( Time2 time )
   {
      
      this( time.getHour(), time.getMinute(), time.getSecond() );
   } 
  
   public void setTime( int h, int m, int s )
   {
      hour =  setHour( h );   // set the hour
      minute= setMinute( m ); // set the minute
      second= setSecond( s ); // set the second
   } 

   public int setHour( int testhour ) 
   { 
      if    ( testhour >= 0 && testhour < 24 ) 
    	  return testhour;
      else 
      { 
         System.out.printf( 
            "Invalid hour (%d) set to 1.  ", testhour );
         return 1; 
      } 
   } 
   public int setMinute( int testminute ) 
   { 
      if (testminute >=0 && testminute <60 )
    	  return testminute;
      else
      {
    	  System.out.printf( 
    	            "Invalid minute (%d) set to 1.   ", testminute );
    	         return 1;
      }
   } 
   public int setSecond( int testsecond ) 
   { 
	   if (testsecond >=0 && testsecond <60 )
	    	  return testsecond;
	      else
	      {
	    	  System.out.printf( 
	    	            "Invalid second (%d) set to 1.   ", testsecond );
	    	         return 1; 
	      }
   }

   public int getHour() 
   { 
      return hour; 
   } // end method getHour

   // get minute value
   public int getMinute() 
   { 
      return minute; 
   } // end method getMinute

   // get second value
   public int getSecond() 
   { 
      return second; 
   } // end method getSecond

   // convert to String in universal-time format (HH:MM:SS)
   public String toUniversalString()
   {
      return String.format( 
         "%02d:%02d:%02d", getHour(), getMinute(), getSecond() );
   } 
  
   public String toString()
   {
      return String.format( "%d:%02d:%02d %s", 
         ( (getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12 ),
         getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ) );
   } 
   public static void main(String[] args){
	     Time2 time = new Time2 (25,61,4);
	//  System.out.print(time);
   } 
}

