import java.util.Scanner;


public class DateAndTime{
	private int month; // 1-12
	private int day;   // 1-31 based on month
	private int year;  // any year
	private int hour;   // 0 - 23
	private int minute; // 0 - 59
    private int second; // 0 - 59
    
    public DateAndTime(){
    	setDate(1,1,2000);
    	setTime(0,0,0);
    }
    
    public DateAndTime(int theMonth, int theDay, int theYear, int theHour, int theMinute, int theSecond){
    	setDate(theMonth, theDay, theYear);
    	setTime(theHour, theMinute, theSecond);
    }
    
    public void setDate( int theMonth, int theDay, int theYear ){
    	month = checkMonth( theMonth );
    	day = checkDay( theDay );
    	year = checkYear( theYear );
    }
    
    public void setTime( int h, int m, int s )
	   {
	      hour =  setHour( h );   // set the hour
	      minute = setMinute( m ); // set the minute
	      second = setSecond( s ); // set the second
	      
	   } 
    
    public int setHour( int h ) 
    { 
       hour = ( ( h >= 0 && h < 24 ) ? h : 0 ); 
       return hour;
    } // end method setHour

    // validate and set minute 
    public int setMinute( int m ) 
    { 
       minute = ( ( m >= 0 && m < 60 ) ? m : 0 ); 
       return minute;
    } // end method setMinute

    // validate and set second 
    public int setSecond( int s ) 
    { 
       second = ( ( s >= 0 && s < 60 ) ? s : 0 ); 
       return second;
    } // end method setSecond

    // Get Methods
    // get hour value
    public int getHour() 
    { 
       return hour; 
    } // end method getHour

    // get minute value
    public int getMinute() 
    { 
       return minute; 
    } // end method getMinute

    // get second value
    public int getSecond() 
    { 
       return second; 
    } // end method getSecond
    
    public void tick(){
    	setSecond( second + 1 );
    	if ( second == 0 )
    		incrementMinute();
    	
    }
    
    public void incrementMinute(){
    	setMinute( minute + 1 );
    	if(minute == 0)
    		incrementHour();
    	
    }
    public void incrementHour(){
    	setHour (hour + 1);
    	if(hour == 0)
    		nextDay();
    	
    }
   
    private int checkYear(int testYear){
    	if (testYear > 0)
    		return testYear;
    	else{
    		System.out.printf("Invalid year (%d) set to 1.\n", testYear);
    		return 1;
    	}
    	
    }
    
    private int checkMonth(int testMonth){
    	if (testMonth > 0 && testMonth <= 12)
    		return testMonth;
    	else{
    		System.out.printf("Invalid month (%d) set to 1.\n", testMonth);
    		return 1;
    	}
    	
    }
   
    public int checkDay( int testDay )
    {
       int daysPerMonth[] =  { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
       if ( testDay > 0 && testDay <= daysPerMonth[ month ] )
          return testDay;
       if ( month == 2 && testDay == 29 && ( year % 400 == 0 || 
            ( year % 4 == 0 && year % 100 != 0 ) ) )
          return testDay;
    
       System.out.printf( "Invalid day (%d) set to 1.", testDay );
       return 1; 
    }
    
    public void nextDay()
    {
 	   int testDay = day +1 ;
 	   if (checkDay(testDay) == testDay)
 		   day = testDay;
 	   else{
 		   day = 1;
 		   nextMonth();
 		   
 	   }
    }	
    
    public void nextMonth(){
    	if (12 == month)
    		year++;
    	
    	month = month % 12 + 1;
    }
    public String toUniversalString()
	   {
	          return String.format( "%d/%d/%d: %02d:%02d:%02d",
	    month, day, year, getHour(), getMinute(), getSecond() );
	    }
	   public String toString()
	   {
		   
	      return String.format("%d/%d/%d: %d:%02d:%02d %s",  month, day, year,
	         ( (getHour() == 0 || getHour() == 12   ) ? 12 : getHour() % 12 ),
	         getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ),"%d/%d/%d" );
	   } 
	   public static void main( String args[] )
	   {
	    Scanner input = new Scanner( System.in );

	    System.out.println( "Enter the date and time" );
	    System.out.print( "Month: " );
	    int month = input.nextInt();
	    System.out.print( "Day: " );
	    int day = input.nextInt();
	    System.out.print( "Year: " );
	    int year = input.nextInt();

	    System.out.print( "Hours: " );
	    int hour = input.nextInt();
	    System.out.print( "Minutes: " );
	    int minute = input.nextInt();
	    System.out.print( "Seconds: " );
	    int seconds = input.nextInt();

	    DateAndTime dateTime = new DateAndTime(
	    month, day, year, hour, minute, seconds );

	    int choice = getMenuChoice();

	    while ( choice != 7 )
	    {
	    switch ( choice )
	    {
	    case 1: // add 1 second
	    dateTime.tick();
	    break;

	    case 2: // add 1 minute
	    dateTime.incrementMinute();
	    break;

	    case 3: // and 1 hour
	    dateTime.incrementHour();
	    break;

	    case 4: // add 1 day
	    dateTime.nextDay();
	    break;

	    case 5: // add 1 month
	    dateTime.nextMonth();
	    break;

	    case 6: // add arbitrary seconds
	    System.out.print( "Enter seconds to tick: " );
	    int ticks = input.nextInt();

	    for ( int i = 0; i < ticks; i++ )
	    dateTime.tick();

	    break;
	    } // end switch34

	    System.out.printf( "Universal date and time: %s\n",
	    dateTime.toUniversalString() );
	    System.out.printf( "Standard date and time: %s\n",
	    dateTime.toString() ); 

	    choice = getMenuChoice();
	    } // end while 
	    } // end main

	    // prints a menu and returns a value corresponding to the menu choice
	    private static int getMenuChoice()
	    {
	    Scanner input = new Scanner( System.in );

	    System.out.println( "1. Add 1 second" );
	    System.out.println( "2. Add 1 Minute" );
	    System.out.println( "3. Add 1 Hour" );
	    System.out.println( "4. Add 1 Day" );
	    System.out.println( "5. Add 1 Month" );
	    System.out.println( "6. Add seconds" );
	    System.out.println( "7. Exit" );
	    System.out.print( "Choice: " );

	    return input.nextInt();
	    } // end method getMenuChoice
} 
	 

    
   
	   