public class Time2
{
   private int hour;   // 0 - 23
   private int minute; // 0 - 59
   private int second; // 0 - 59

   public Time2()
   {
      this( 0, 0, 0 ); // invoke Time2 constructor with three arguments
   } 
   public Time2( int h ) 
   { 
      this( h, 0, 0 ); 
   } 
   public Time2( int h, int m ) 
   { 
      this( h, m, 0 ); 
   } 
   
   public Time2( int h, int m, int s ) 
   { 
      setTime( h, m, s ); 
     
      System.out.printf( "Date object constructor for date %s\n", this );
   } 

   public Time2( Time2 time )
   {
      this( time.getHour(), time.getMinute(), time.getSecond() );
   } 
  
   public void setTime( int h, int m, int s )
   {
      hour =  setHour( h );   // set the hour
      minute= setMinute( m ); // set the minute
      second= setSecond( s ); // set the second
      
   } 

   public int setHour( int testhour ) 
   { 
      if    ( testhour >= 0 && testhour < 24 ) 
    	  return testhour;
      else 
      { 
         return 0; 
      }
   } 
   public int setMinute( int testminute ) 
   { 
      if (testminute >=0 && testminute <60 )
    	  return testminute;
      else
      {
    	  return -1;
      }
   } 
   public int setSecond( int testsecond ) 
   { 
	   if (testsecond >=0 && testsecond < 60 )
	    	  return testsecond;
	      else
	      {
	    	  return -1; 
	      }
   }

   
   public void tick()
   {
	   second++;
	   if (second==60) {
		   second=0;
		   incrementMinute();
	   }
	   }
   
   public void incrementMinute()
   {
	   minute++;
	   if(minute==60) {
		   minute=0;
		   incrementHour();
	  }
   }
   public void incrementHour (){
	   if (++hour == 24){
		   hour = 0;
	   }
   }
   
  
   public int getHour() 
   { 
      return hour; 
   } 
   public int getMinute() 
   { 
      return minute; 
   } 
   public int getSecond() 
   { 
      return second; 
   } 
   public String toUniversalString()
   {
      return String.format( 
         "%02d:%02d:%02d", getHour(), getMinute(), getSecond() );
   } 
  
   public String toString()
   {
      return String.format( "%d:%02d:%02d %s", 
         ( (getHour() == 0 || getHour() == 12   ) ? 12 : getHour() % 12 ),
         getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ) );
   } 
   public static void main(String[] args){
	     Time2 time = new Time2 (23,59,59);
	     time.tick();
	     System.out.println (time.toString());
	     time.incrementMinute();
	     System.out.println (time.toString());
	     time.incrementHour();
	     System.out.println (time.toString());
	     
   } 
}

