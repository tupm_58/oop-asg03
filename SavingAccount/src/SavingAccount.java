public class SavingAccount {
    static double annualInterestRate = 0.00;
    private double savingsBalance;

     public SavingAccount() {
          savingsBalance = 0;
          annualInterestRate = 0;
    }
 
     public SavingAccount(double initBalance) {
   
               savingsBalance = initBalance;
   
    }

    static void modifyInterestRate(double interestRate) {
          annualInterestRate = interestRate;
    }

   public double calculateMonthlyInterest() {
        double money =savingsBalance;
        savingsBalance +=((savingsBalance * annualInterestRate) / 12);
		return savingsBalance-money;
		
   }
    public double getBalance() {
       return savingsBalance;
   }
   
	   public static void main(String args[]) {
       
        SavingAccount saver1 = new SavingAccount(2000);
        SavingAccount saver2 = new SavingAccount(3000);
		
        SavingAccount.modifyInterestRate(0.04);
        
        System.out.println("AnnualInterestRate is 4%: \n");
        System.out.println("Saver1 calculateMonthlyInterest: $" + saver1.calculateMonthlyInterest());
		System.out.println("Saver1 balance: $" + saver1.getBalance());
		
        System.out.println("Saver2 calculateMonthlyInterest: $" + saver2.calculateMonthlyInterest());
		System.out.println("Saver2 balance: $" + saver2.getBalance() + "\n\n");
		
		

        SavingAccount.modifyInterestRate(0.05);
            
        System.out.println("AnnualInterestRate is 5%: \n");
        System.out.println("Saver1 balance: $" + saver1.getBalance());
		System.out.println("Saver1 calculateMonthlyInterest: $" + saver1.calculateMonthlyInterest());
        System.out.println("Saver2 balance: $" + saver2.getBalance());
		System.out.println("Saver2 calculateMonthlyInterest: $" + saver2.calculateMonthlyInterest() + "\n");
	}
}
	 